var current_layout = "";
var agent = navigator.userAgent.toLowerCase();
var is_ie = (agent.indexOf("msie") != -1) && document.all;
var delay_hide_id;
function enable_keyboard(id){
	var node = document.getElementById(id);
	if(node.nodeType==1){
		if (node.addEventListener){
  			node.addEventListener('focus', show_keyboard, false);
			node.addEventListener('blur', delay_hide_keyboard, false); 
			node.addEventListener('keypress',handle_keypress,false);
		} else if (node.attachEvent){
  			node.attachEvent('onfocus', show_keyboard);
			node.attachEvent('onblur', delay_hide_keyboard);
			node.attachEvent('onkeypress',handle_keypress);
		}
		initialize_keyboard_support();
	}	
}
function handle_keypress(ev){
	ev = ev || window.event;
	if(ev.bubbles!=null && !ev.bubbles)
		return true;
	var target   = ev.target || ev.srcElement;
	var kb = document.getElementById('_divkeyboard');
	var keyCode = window.event?ev.keyCode:ev.which;
	//document.getElementById('divDisplay').innerHTML+="key press: "+keyCode+"<br />";
	if(keyCode==0)
		return true;
	var newKeyCode = map_key_code(keyCode);
	if(newKeyCode==keyCode)
		return true;
	if(target.addEventListener){
		//cancel the event
		ev.preventDefault();
		ev.stopPropagation();
		//create new event with the keycode changed
		var evt = document.createEvent("KeyboardEvent");
		evt.initKeyEvent("keypress", false, true, document.defaultView, ev.ctrlKey, ev.altKey, ev.shiftKey, ev.metaKey, newKeyCode, newKeyCode);
		//dispatch new event
		target.dispatchEvent(evt);
	}
	else if(target.attachEvent){
		ev.keyCode = keyCode;
	}
    return false;
}
function map_key_code(keyCode){
	if(kb_layouts[current_layout] && kb_layouts[current_layout][keyCode])
		return kb_layouts[current_layout][keyCode];
	return keyCode;
}

function initialize_keyboard_support(){
	var kb = document.getElementById('_divkeyboard');
	var lo = document.getElementById('_selkeyboardlayouts');
	if(lo.options.length>0)
		return;
	var i = 1;
	lo.options[i]=new Option();
	lo.options[i].value="";
	lo.options[i].text=" ";
	for(var lang in kb_layouts){
		lo.options[i]=new Option();
		lo.options[i].value = lang;
		lo.options[i++].text = _names[lang];
	}		
}
function change_keyboard_layout(ev){
	ev = ev || window.event;
    var target   = ev.target || ev.srcElement;
	current_layout = target.value;
	var mappings = document.getElementById('_divkeyboardmappings');
    mappings.innerHTML="";
	if(kb_layouts[current_layout]){
		var br = 0;
		for(key in kb_layouts[current_layout]){
			if(mappings.innerHTML=="")
				mappings.innerHTML+= _names['press']+" "+String.fromCharCode(key)+" "+ _names['for'] + " "+String.fromCharCode(kb_layouts[current_layout][key]);
			else
				mappings.innerHTML+= _names['press']+" "+String.fromCharCode(key)+" "+ _names['for'] + " "+String.fromCharCode(kb_layouts[current_layout][key]);
			br++;
			//if((br % 2)==0)
				mappings.innerHTML+="<br />";
			//else
			//	mappings.innerHTML+=", ";
		}
	}
}
function show_keyboard_layout(ev){
	cancel_delay_hide_keyboard();
	var kb = document.getElementById('_divkeyboard');
	kb.style.visibility="visible";
	//document.getElementById('divDisplay').innerHTML+="show keyboard layout<br />";
	var lo = document.getElementById('_selkeyboardlayouts');
	lo.focus();
}
function show_keyboard(ev){
	ev = ev || window.event;
	var target = ev.target || ev.srcElement;
	cancel_delay_hide_keyboard();
	//document.getElementById('divDisplay').innerHTML+="show keyboard<br />";
	var kb=document.getElementById('_divkeyboard');
	kb.style.visibility="visible";
	var left=get_element_left_position(target);
	var top=get_element_top_position(target);
	if((left+target.offsetWidth + kb.offsetWidth+5)>document.body.offsetWidth){
		top+=(target.offsetHeight + 5);
	}
	else{
		left+=target.offsetWidth+5;
		top-=5;
	}
	kb.style.top=top+"px";
	kb.style.left=left+"px";
	return false;
}
function hide_keyboard(ev){
	var kb = document.getElementById('_divkeyboard');
	kb.style.visibility="hidden";
	return false;
}
function delay_hide_keyboard(ev){
    delay_hide_id = window.setTimeout(hide_keyboard,500);
}
function cancel_delay_hide_keyboard(){
	 if(delay_hide_id>=0)
        window.clearTimeout(delay_hide_id);
	delay_hide_id=-1;
}

function get_element_top_position(element){
	var top=0;
	if(element.offsetParent){
		while(element.offsetParent)
		{
			top+=element.offsetTop;
			element=element.offsetParent;
		}
		top+=element.offsetTop;
	}
	else if(element.y){
		top+=element.y;
	}
	return top;
}
function get_element_left_position(element){
	var left=0;
	if(element.offsetParent){
		while(element.offsetParent)
		{
			left+=element.offsetLeft;
			element=element.offsetParent;
		}
		left+=element.offsetLeft;
	}
	else if(element.x){
		left+=element.x;
	}
	return left;
}

document.write("<div id='_divkeyboard' style='position:absolute; z-index:10; visibility: hidden; background-color:white;border:solid 1px #dddddd;top:10px;width:100px;padding:5px'>"+help+"<select id='_selkeyboardlayouts' onfocus='show_keyboard_layout();' onblur='delay_hide_keyboard(event);' onchange='change_keyboard_layout(event);' style='width:100px'></select><div id='_divkeyboardmappings' ></div><a href='http://kasahorow.com/help/keyboard' target='kasahorow'>"+khelp+"</a></div>");
